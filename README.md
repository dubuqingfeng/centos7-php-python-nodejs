#为PHP、Python、NodeJS开发者打造的Docker快速部署测试服务器镜像
---

为一些Web开发者打造的服务器部署镜像，主要面向PHP、Python、NodeJS开发者，集成了composer，PHP常用框架的支持，以及pip、mysql、mongodb等服务。主要面向测试环境，方便部署服务器。并且可自由地定制，在测试环境通过后，建立安全基础设施后，可部署上线。
##0x01 Docker简单介绍
###利用Docker的场景.
1.在传统开发及测试当中，开发需要和测试进行一定的沟通，从而争取开发环境与测试环境的统一，但不能保证完全相同，以及还有服务器生产环境。版本配置等的不同,导致了一些部署上的问题。利用Docker，开发部门只需打包好镜像，测试及运维部门通过Docker快速部署，可以规定一些基础服务的版本等，只需要几分钟可以配置好测试及运维环境。

2.沙箱可以实现轻型隔离，多个容器间不会相互影响。

3.大规模集群部署。

4.Docker可以自动化打包和部署任何应用，方便地创建一个轻量级私有PaaS云。

###什么是Docker？
[Docker](https://www.docker.com/)是一个开放源代码软件专案，在软件容器下自动布署应用程序，借此在Linux操作系统上，提供了一个额外的软件抽象层，以及操作系统层虚拟化的自动管理机制。
###能用Docker做什么？
集群测试环境、大规模Web部署、数据库集群、持续部署系统、私有PaaS技术，面向服务的体系结构……

+ Docker可以应用在一些[集群部署](http://www.infoq.com/cn/articles/tencent-millions-scale-docker-application-practice)的场景，方便地构建[集群](http://www.infoq.com/cn/articles/large-scale-docker-cluster-practise-experience-share)架构。
+ [私有PaaS技术](http://qing.blog.sina.com.cn/2294942122/88ca09aa33003ydp.html)
+ 而对于一些中小型的创业企业或者互联网公司的开发者来说，一个稳定的[测试环境](http://www.xiaomastack.com/2015/04/05/docker-dev-test-deploy/)是重要的，尤其是开发部门和测试、运维部门的配合方面，开发部门只需维护一个[Dockerfile](https://docs.docker.com/reference/builder/)，可以统一开发环境并减低运维团队负担。

###Docker有什么优势？
[Docker之二三事](http://www.xiaozhou.net/something-about-docker-2014-05-30.html)
###Docker如何方便地入门？
+ [Docker入门与实践](https://www.gitbook.com/book/yeasy/docker_practice/details)
+ [Docker入门实战](http://yuedu.baidu.com/ebook/d817967416fc700abb68fca1)
+ [官方文档](https://docs.docker.com/)
+ [第一本Docker书](http://product.dangdang.com/23623098.html)
+ Docker几个概念要了解，镜像、容器、仓库
+ [「开发工具，开发模式」Docker 入门教程](http://www.html-js.com/qa/Docker-tutorial)

##0x02 如何部署本镜像
###版本说明
基于CentOS7，~~其他版本的镜像在[]()。~~

+ Ubuntu版本的Dockerfile在[Github](https://github.com/dubuqingfeng/Docker-Web-Images)，镜像构建仓库在紧急测试中。

###部署说明
1.安装并测试Docker，可以参考[官方文档](https://docs.docker.com/installation/ubuntulinux/)。或者是《Docker入门与实践》[安装](http://yeasy.gitbooks.io/docker_practice/content/install/index.html)一节。

2.通过拉取镜像，配置服务。

	sudo docker pull index.alauda.cn/dubuqingfeng/centos7-php-python-nodejs

3.运行容器

	sudo docker run -p 80:80 -p 1337:1337 -p 8080:8080 index.alauda.cn/dubuqingfeng/centos7-php-python-nodejs

4.进入容器

	sudo docker attach 容器id
	
容器id，可以使用`docker ps -a`查看。

5.测试应用

PHP可以打开`http://ip地址:80`

Python可以打开`http://ip地址:8080`

~~NodeJS可以打开`http://ip地址:1337`~~

详细测试与应用可在*容器使用*一节

6.测试完毕后删除容器

	sudo docker rm  容器id

7.删除镜像

可以使用 `docker rmi `命令。注意 `docker rm `命令是移除容器。

	sudo docker rmi index.alauda.cn/dubuqingfeng/centos7-php-python-nodejs

**注意**：在删除镜像之前要先用 `docker rm `删掉依赖于这个镜像的所有容器。

###集成的服务
+ Nginx
+ PHP-FPM
+ Python
+ Django
+ NodeJS
+ npm
+ composer
+ MySql
+ Mongodb
+ pip
+ ……

其他版本的Dockerfile在[这里](https://github.com/dubuqingfeng/Docker-Web-Images)，由于本镜像较大，可以参考自己的服务，进行自由定制镜像，从而自己维护镜像，参考**自由定制化**一节。
##0x03 容器使用
###PHP

	sudo docker run -d -P 80:80 --name web -v /src/webapp:/var/www index.alauda.cn/dubuqingfeng/centos7-php-python-nodejs

####PHP框架测试

集成了phalcon，其余可用composer进行安装。

###Python

	sudo docker run -d -P 8080:8080 --name web -v /src/webapp:/src index.alauda.cn/dubuqingfeng/centos7-php-python-nodejs python app.py

###NodeJS

	sudo docker run -d -P 1337:1337 --name web -v /src/webapp:/src index.alauda.cn/dubuqingfeng/centos7-php-python-nodejs node app.js
	
##0x04 镜像说明
###说明
本镜像是为了PHP、Python、NodeJS开发者建立的服务器快速部署，适用于开发部门测试好以后，提交给测试部门。

使用supervisor进行进程管理。

DevOps：
Docker占用资源小，在一台 E5 128 G 内存的服务器上部署 100 个容器都绰绰有余，可以单独抽一个容器或者直接在宿主物理主机上部署 samba，利用 samba 的 home 分享方案将每个用户的 home 目录映射到开发中心和测试部门的 Windows 机器上。

针对某个项目组，由架构师搭建好一个标准的容器环境供项目组和测试部门使用，每个开发工程师可以拥有自己单独的容器，通过 docker run -v 将用户的 home 目录映射到容器中。需要提交测试时，只需要将代码移交给测试部门，然后分配一个容器使用 -v 加载测试部门的 home 目录启动即可。这样，在公司内部的开发、测试基本就统一了，不会出现开发部门提交的代码，测试部门部署不了的问题。

测试部门发布测试通过的报告后，架构师再一次检测容器环境，就可以直接交由部署工程师将代码和容器分别部署到生产环境中了。这种方式的部署横向性能的扩展性也极好。
###本镜像基础
本镜像基于Centos7，使用Nginx作为服务器，mysql或者mongodb作为数据库，PHP、Python、NodeJS作为后端开发语言。
###自由定制化
可以自由定制一些镜像，例如：

+ [centos7-mongodb](https://www.alauda.cn/cp/dubuqingfeng#/repo/detail?repo=dubuqingfeng%2Fcentos7-mongodb):docker pull index.alauda.cn/dubuqingfeng/centos7-mongodb
+ [centos7-nginx-php](https://www.alauda.cn/cp/dubuqingfeng/#/repo/detail?repo=dubuqingfeng%2Fcentos7-nginx-php) :docker pull index.alauda.cn/dubuqingfeng/centos7-nginx-php
+ [centos7-nodejs](https://www.alauda.cn/cp/dubuqingfeng/#/repo/detail?repo=dubuqingfeng%2Fcentos7-nodejs):docker pull index.alauda.cn/dubuqingfeng/centos7-nodejs
+ [centos7-nodejs-bower-grunt](https://www.alauda.cn/cp/dubuqingfeng/#/repo/detail?repo=dubuqingfeng%2Fcentos7-nodejs-bower-grunt):docker pull index.alauda.cn/dubuqingfeng/centos7-nodejs-bower-grunt
+ [centos7-nodejs-express](https://www.alauda.cn/cp/dubuqingfeng/#/repo/detail?repo=dubuqingfeng%2Fcentos7-nodejs-express):docker pull index.alauda.cn/dubuqingfeng/centos7-nodejs-express
+ [centos7-nodejs-mongodb-express](https://www.alauda.cn/cp/dubuqingfeng/#/repo/detail?repo=dubuqingfeng%2Fcentos7-nodejs-mongodb-express):docker pull index.alauda.cn/dubuqingfeng/centos7-nodejs-mongodb-express
+ [centos7-python-django](https://www.alauda.cn/cp/dubuqingfeng/#/repo/detail?repo=dubuqingfeng%2Fcentos7-python-django):docker pull index.alauda.cn/dubuqingfeng/centos7-python-django
+ [docker-web-python](https://www.alauda.cn/cp/dubuqingfeng/#/repo/detail?repo=dubuqingfeng%2Fdocker-web-python):docker pull index.alauda.cn/dubuqingfeng/docker-web-python

###打包镜像
####在此镜像基础上如何进一步打包镜像？

#####1.根据本镜像的Dockerfile，维护自己的Dockerfile。参考官方文档的[Dockerfile一节](https://docs.docker.com/reference/builder/)。

#####2.拉取本镜像，提交到自己的仓库里。

这一部分内容可参考[如何使用AlaudaCloud的Docker镜像服务](http://blog.alauda.cn/?p=38)。

使用 docker push 上传标记的镜像。

#####3.使用本地文件导入镜像，再进行导出。

可以使用 docker load 从导出的本地文件中再导入到本地镜像库，例如

	sudo docker load --input centos7-php-python-nodejs.tar

或

	sudo docker load < centos7-php-python-nodejs.tar

####导出或者转储镜像

	sudo docker save -o centos7-php-python-nodejs.tar index.alauda.cn/dubuqingfeng/centos7-php-python-nodejs

###本地化虚拟环境
可以和Vagrant一块构建开发环境，具体参考[使用 Vagrant 和 Docker 在一个 VM 中进行开发](http://www.oschina.net/translate/unsuck-your-vagrant-developing-in-one-vm-with-vagrant-and-docker)和[1+1>2:用Docker和Vagrant构建简洁高效开发环境](http://cloud.51cto.com/art/201503/470256.htm)。

Docker和Vagrant经常被认为是两种相互替代的工具，其实它们可以结合使用，构建隔离的、可重复的开发环境。我们将证明该环境可以构建一个Docker容器以便开发Java应用程序，并充分利用Vagrant的强大功能，以解决一些现实当中的实际问题。

##0x05 服务器安全配置总结
最小的权限控制，最小的服务。

####1.更换VPS SSH端口
####2.使用iptables建立基础安全设施
####3.避免使用root用户
####4.建立docker用户组
####5.可以尝试使用selinux
####6.配置安全狗、云盾等服务
####7.使用密钥登录ssh
####8.注意安全漏洞，例如bash、apache、vsftpd、nginx、PHP、mysql、mongodb等的。
####9.加强人员安全配置意识。
####10.关闭不必要的服务
####待整理

##0x06 反馈
###提交反馈
如有问题及建议，请提交issue到[Github](https://github.com/dubuqingfeng/Docker-Web-Images)。或者发送email到1135326346@qq.com。
###协议
开源协议: GNU General Public Licence v3
###更新记录
v1.0.0

+ 建立Dockerfile
+ 测试各组件服务
+ 集成测试，并在vps上测试。

###TODO

+ 写ubuntu版本的dockerfile
+ 思考更多的安全与配置服务

###说明
本镜像是在近些天的学习中构建的，为了方便国内NodeJS开发者进行服务器的快速部署，同时尽可能将文档写得易懂，照顾基础不是很好的进行理解，在学习的过程中不免会有一些考虑不周到的方面，希望继续交流。

关于这个镜像的其他方面，还有一些问题，例如安全，自动化运维。。这里进行一些思考，并且做了一些实践。

在安全方面，因为Docker是一种沙箱机制。也得考虑安全问题，假如从容器突破到宿主主机、由于权限为root用户，最好实际上线时，做到业务分离，经常查看日志，同时做好web应用的审计等工作。

##0x07 参考资料
###nginx.conf配置
[How to run nginx within docker container without halting?](http://stackoverflow.com/questions/18861300/how-to-run-nginx-within-docker-container-without-halting)

[Nginx.conf介绍](http://www.cnblogs.com/zemliu/archive/2012/10/14/2723187.html)

###nginx.conf default.conf配置
[Getting Started with Docker](https://serversforhackers.com/getting-started-with-docker)

[Nginx Configuration Examples](http://kbeezie.com/nginx-configuration-examples/)
###关于CGI,FastCGI，php-fpm
[Setting Up PHP behind Nginx with FastCGI](http://www.sitepoint.com/setting-up-php-behind-nginx-with-fastcgi/)

[什么是CGI、FastCGI、PHP-CGI、PHP-FPM、Spawn-FCGI？](http://www.mike.org.cn/articles/what-is-cgi-fastcgi-php-fpm-spawn-fcgi/)
###Nginx在centos7的坑(systemctl)

[How To Install Nginx on CentOS 7](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-centos-7)

###php-ldap扩展的安装问题
###phalcon安装配置

[Phalcon安装和配置](http://blog.jingwentian.com/post/phalcon/phalconan-zhuang-he-pei-zhi)

###Docker安全机制

[Docker服务端的防护](http://dockerpool.com/static/books/docker_practice/security/daemon_sec.html)

###supervisor
[官方文档](https://docs.docker.com/articles/using_supervisord/)

[为什么不需要在 Docker 容器中运行 sshd](http://www.oschina.net/translate/why-you-dont-need-to-run-sshd-in-docker)

[CentOS 7 下 docker 安装拾穗](http://best33.com/144.moe)

[sel实验室](http://www.sel.zju.edu.cn/)

[以 FIG 快速建構 DOCKER 的多容器環境](https://blog.mikuru.tw/archives/429)

[使用 Docker 部署 Python 应用的一些经验总结](http://codecloud.net/docker-python-3828.html)

